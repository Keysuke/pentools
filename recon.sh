#!/bin/bash

BASE_DIR=$PWD/output
TARGET_FILE=./targets.txt

if [ -r $TARGET_FILE ]; then
	mapfile -t TARGETS < $TARGET_FILE
else
	echo "Could not read $TARGET_FILE. Aborting."
	exit 1
fi

PASSIVE_RECON_DIR=$BASE_DIR/passive_recon
ACTIVE_RECON_DIR=$BASE_DIR/active_recon
SCAN_DIR=$BASE_DIR/scan

mkdir -p $PASSIVE_RECON_DIR/{host,whois_domain,whois_ip,dig_any,dnsrecon}
mkdir -p $ACTIVE_RECON_DIR/{nmap_ports_tcp,nmap_A_tcp,nmap_ports_udp,wafw00f,traceroute}
mkdir -p $SCAN_DIR/{nikto,gobuster,nmap_methods}

for target in "${TARGETS[@]}"; do
	echo "Processing $target"
	IP=$(host $target|grep " has address "|cut -d" " -f4)
	TOP_DOMAIN=$(echo $target | rev | cut -d . -f 1-2 | rev)
	echo "$target has IP $IP"
	echo "Top domain is $TOP_DOMAIN"
	
	host $target > $PASSIVE_RECON_DIR/host/$target
	whois $IP > $PASSIVE_RECON_DIR/whois_ip/$target
	if [ ! -f $PASSIVE_RECON_DIR/whois_domain/$TOP_DOMAIN ]; then
		whois $TOP_DOMAIN > $PASSIVE_RECON_DIR/whois_domain/$TOP_DOMAIN
		dig ANY $TOP_DOMAIN > $PASSIVE_RECON_DIR/dig_any/$TOP_DOMAIN
		dnsrecon -d $TOP_DOMAIN -t axfr > $PASSIVE_RECON_DIR/dnsrecon/$TOP_DOMAIN
	fi

	wafw00f -v -a "https://$target" &> "$ACTIVE_RECON_DIR/wafw00f/$target"
	wafw00f -v -a "$target" &> "$ACTIVE_RECON_DIR/wafw00f/nossl_$target"

	traceroute $target > $ACTIVE_RECON_DIR/traceroute/$target
	nmap -sS -p 80,443 $target -script=http-methods -oN $SCAN_DIR/nmap_methods/$target -Pn
	nmap -sS -p- $target -oN $ACTIVE_RECON_DIR/nmap_ports_tcp/$target -Pn

	nikto -h https://$target/ -output $SCAN_DIR/nikto/$target.txt
	nikto -h http://$target/ -output $SCAN_DIR/nikto/no_ssl_$target.txt

	gobuster dir -u https://$target/ -w /usr/share/dirbuster/wordlists/directory-list-2.3-medium.txt -o $SCAN_DIR/gobuster/$target -r
	nmap -sU -p- $target -oN $ACTIVE_RECON_DIR/nmap_ports_udp/$target -Pn
	nmap -sS -A --reason -p- $target -oN $ACTIVE_RECON_DIR/nmap_A_tcp/$target -Pn
	echo "Recon for $target ended"
done
